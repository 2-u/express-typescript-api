# Boilerplate Typescript API server

## Prerequisites

- Basic Linux command line skills
- NodeJS (currently on v20.2.0)
- Typescript
- nvm (Node Version Manager): to install different NodeJS versions

Optional, but highly recommended:
- Docker
- k3s (small kubernetes cluster)
- terraform

## Quickstart

To start the server locally with docker compose (RECOMMENDED)

```
$ sudo ./startserverLocalDockerCompose.sh
```

To start the server with docker

```
$ sudo ./startserverLocalDocker.sh
```

To stop the server with docker

```
$ sudo ./stopserverLocalDocker.sh
```

To tail the logs

```
$ sudo docker logs -f my-ts-api
```

## INSTRUCTIONS FROM SCRATCH: Install -> Start Server -> Test Endpoint -> Stop Server

```
$ mkdir express-typescript-api
$ cd express-typescript-api/
$ npm init  					# accept defaults. creates package.json
$ npm install express
$ npm install express-graphql
$ npm install graphql@15.3.0			# you need this version for express-graphql
$ npm install --save-dev typescript    		# save these as dev dependencies
$ npm install --save-dev @types/node   		# save these as dev dependencies
$ npm install --save-dev @types/express  	# save these as dev dependencies


---
$ cat package.json 
{
  "name": "express-typescript-api",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "",
  "license": "ISC",
  "dependencies": {
    "express": "^4.18.2"
  },
  "devDependencies": {
    "@types/express": "^4.17.17",
    "@types/node": "^20.4.0",
    "typescript": "^5.1.6"
  }
}

---
$ npx tsc --init				# create tsconfig file

Created a new tsconfig.json with:
  target: es2016
  module: commonjs
  strict: true
  esModuleInterop: true
  skipLibCheck: true
  forceConsistentCasingInFileNames: true

You can learn more at https://aka.ms/tsconfig

---
$ mkdir build src

---
# open tsconfig.json and add these 2 lines (or find them in the file, uncomment them and edit the values):
"outDir": "./build",
"rootDir": "./src",

---
$ vi src/app.ts  				# create file and paste in the text below:
import express, { Application, Request, Response } from 'express';

const app: Application = express();

const PORT: number = 3001;

app.use('/', (req: Request, res: Response): void => {
    res.send('Hello world!');
});

app.listen(PORT, (): void => {
    console.log('SERVER IS UP ON PORT:', PORT);
});

---
$ npx tsc  					# this compiles the typescript code and writes app.js to the build dir
$ cat build/app.js 
"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const app = (0, express_1.default)();
const PORT = 3001;
app.use('/', (req, res) => {
    res.send('Hello world!');
});
app.listen(PORT, () => {
    console.log('SERVER IS UP ON PORT:', PORT);
});

---
$ node build/app.js &  				# start the server in the background
[1] 357904
$ SERVER IS UP ON PORT: 3001

$ curl http://localhost:3001  # hit the endpoint. it should return 'Hello world!'
Hello world!

---
$ ps -ef | grep app.js  			# stop the server by finding the pid of app.js and killing it
m         357904  355236  0 20:54 pts/3    00:00:00 node build/app.js
m         357956  355236  0 20:55 pts/3    00:00:00 grep --color=auto app.js
$ kill -9 357904
$ ps -ef | grep app.js  			# find it again to confirm that server is killed
m         357972  355236  0 20:55 pts/3    00:00:00 grep --color=auto app.js
[1]+  Killed                  node build/app.js
```
