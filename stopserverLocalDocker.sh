#!/bin/bash

set -euxo pipefail

# IMPORTANT! Run this script as sudo
# $ sudo ./stopserverLocalDocker.sh

# stop the server
docker stop my-ts-api

# remove the container name so that you can reuse it
docker rm my-ts-api
