# Provision an EKS Cluster with Terraform

https://developer.hashicorp.com/terraform/tutorials/kubernetes/eks

## Prerequisites

- Terraform
- AWS CLI

## Quickstart

Deploy EKS to AWS with terraform (takes about 15 minutes)

```
$ terraform apply
```

Configure your kubectl to use your local aws credentials to access your EKS cluster

```
aws eks --region $(terraform output -raw region) update-kubeconfig  --name $(terraform output -raw cluster_name)
```

Check that cluster is accessible and the nodes are up

```
$ kubectl cluster-info
$ kubectl get nodes
```

Destroy EKS resources to when you don't need it (about 15 minutes)

```
$ terraform destroy
```

## Deploy to AWS with Github Actions

IMPORTANT! Make sure to add the AWS secrets in GitHub Actions here:

https://github.com/yourgithubusername/typescript-express-helm-eks/settings/secrets/actions

Just change any file under infra/eks dir and git push to master to trigger GitHub Actions to deploy EKS to AWS

