#!/bin/bash

set -euxo pipefail

# Deploy EKS to AWS
terraform apply

sleep 10

# Configure local kubectl to use local AWS access credentials for sending commands to the EKS cluster
aws eks --region $(terraform output -raw region) update-kubeconfig --name $(terraform output -raw cluster_name)

# check that kubectl works and is connected properly to the EKS cluster
kubectl --kubeconfig=/home/m/.kube/config cluster-info
kubectl --kubeconfig=/home/m/.kube/config get nodes
