import express, { Application, Request, Response } from "express";
import { buildSchema } from "graphql";
import { graphqlHTTP } from "express-graphql";

const app: Application = express();

// TODO: Remove hardcoding of users and get from DB
const users = [
  { id: 1, name: "John Doe", email: "johndoe@gmail.com" },
  { id: 2, name: "Jane Doe", email: "janedoe@gmail.com" },
  { id: 3, name: "Mike Doe", email: "mikedoe@gmail.com" },
];

// BEGIN: build GraphQL schema
const schema = buildSchema(`
    input UserInput {
        email: String!
        name: String!

    }

    type User {
        id: Int!
        name: String!
        email: String!
    }

    type Mutation {
        createUser(input: UserInput): User
        updateUser(id: Int!, input: UserInput): User
    }

    type Query {
        getUser(id: String): User
        getUsers: [User]
    }
`);
// END: build GraphQL schema

// BEGIN: set Typescript types
type User = {
  id: number;
  name: string;
  email: string;
};

type UserInput = Pick<User, "email" | "name">;
// END: set Typescript types

// BEGIN: define resolvers
const getUser = (args: { id: number }): User | undefined =>
  users.find((u) => u.id === args.id);

const getUsers = (): User[] => users;

const createUser = (args: { input: UserInput }): User => {
  const user = {
    id: users.length + 1,
    ...args.input,
  };
  users.push(user);

  return user;
};

const updateUser = (args: { user: User }): User => {
  const index = users.findIndex((u) => u.id === args.user.id);
  const targetUser = users[index];

  if (targetUser) users[index] = args.user;

  return targetUser;
};

const root = {
  getUser,
  getUsers,
  createUser,
  updateUser,
};
// END: define resolvers

const PORT: number = 3001;

// Define GraphQL endpoint
app.use(
  "/graphql",
  graphqlHTTP({
    schema: schema,
    rootValue: root,
    graphiql: true,
  })
);

app.use("/", (req: Request, res: Response): void => {
  res.send("Hello world!");
});

app.listen(PORT, (): void => {
  console.log("SERVER IS UP ON PORT:", PORT);
});
