#!/bin/bash

set -euxo pipefail

# IMPORTANT! Run this script as sudo
# $ sudo ./startserverLocalDockerCompose.sh

# compile the typescript according to settings in tsconfig.json
tsc

# rebuild container and start the server
docker compose up --build --detach

# run integration test
./integrationtest.sh

