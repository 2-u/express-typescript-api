#!/bin/bash
set -euxo pipefail

# sleep for 5 seconds and try to hit a test endpoint with curl
sleep 5
curl http://localhost:3001


# sleep for 5 seconds and try to hit the GraphQL endpoint with curl
sleep 5
#curl http://localhost:3001/graphql
curl 'http://localhost:3001/graphql' \
  -X POST \
  -H 'content-type: application/json' \
  --data '{
    "query": "{ getUsers { id name email} }"
  }'

