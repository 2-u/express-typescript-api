#!/bin/bash

set -euxo pipefail

# IMPORTANT! Run this script as sudo
# $ sudo ./startserverLocalDocker.sh

# compile the typescript according to settings in tsconfig.json
tsc

# build the code (last . at end of command is important)
docker build --tag express-typescript-api .

# start the server on port 3001 with container name 'my-ts-api' using image 'express-typescript-api' and detach
docker run -p 3001:3001 --detach --name my-ts-api express-typescript-api

# sleep for 5 seconds and try to hit a test endpoint with curl
sleep 5
curl http://localhost:3001
